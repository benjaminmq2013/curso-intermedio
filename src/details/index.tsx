import { styled } from "goober";
import { useState } from "react";
import Button from "../component/button"

const Container = styled("div")<{ image: string, visible: boolean }>`
    position: fixed;
    inset: 0;
    z-index: 100;

    width: 100%;
    height: 100vh;

    background-image: url(${props => props.image});
    background-repeat: no-repeat;
    
    background-position: center;
    background-size: cover;
    
    transform: ${props => props.visible ? "translateX(0)" : "translateX(100%)"};
    transition: all .4s;

    .card{
        position: absolute;
        background-color: #0a0a0adc;
        width: 100vw;
        bottom: 0;
        border-radius: 7% 7% 0px 0px;
        padding: 20px;
        backdrop-filter: blur( 4px );
        
        display: flex;
        flex-direction: column;
        gap: 15px;
    }

    .title{
        color: white;
        margin: 0px;
        font-size: 38px;
    }
    .description{
        color: white;
        margin: 0px;
        color: #CFCED1;
    }

    .btn-back{
        background-color: white;
        border: 1px solid white;
        color: #252525;
        width: 30%;
    }

    .button{
        width: 65%;
    }

    .row{
        display: flex;
        flex-direction: row;
        /* justify-content: space-between; */
        align-items: center;
        gap: 15px;
    }

    .counter{
        color: white;
        font-size: 28px;
    }

    .btn-operator{
        background-color: #303030;
        border: 1px solid #303030;
        width: 45px;
    }

    .price{
        color: white;
        font-size: 30px;
        transition: 1s ease-out;
    }
    
`
;
export interface params{
    image: string;
    title: string;
    description: string;
    price: number;
    visible: boolean;
    onClick?: () => void;
}

const App = (params:params):JSX.Element => {

    const [ counter, setCounter ] = useState<number>(0)

    const handleAdd = () => setCounter(counter + params.price)
    const handleSub = () => counter > 0 && setCounter(counter - params.price)

    return(
        <Container image={params.image} visible={params.visible} >
            <div className="card">
                <h2 className="title">{ params.title }</h2>
                <p className="description">Lorem ipsum dolor sit, ame, dicta laboriosam, incidunt accusamus quis repellendus consequuntur nostrum ea eos officiis explicabo illo?</p>
                
                <div className="row">
                    <Button className="btn-operator" label="-" onClick={handleSub} />
                    <p className="counter">{ counter / params.price }</p>
                    <Button label="+" className="btn-operator" onClick={handleAdd} />
                    
                    <p className="price">
                        ${ counter }
                    </p>
                </div>

                <div className="row">
                    <Button label="Pagar" />
                    <Button className="btn-back" label="volver" onClick={params.onClick} />
                </div>

                
            </div>
        </Container>
    )

}
export default App;