import { styled } from "goober";
import { CSSProperties } from "react";

const Container = styled("button")`
    border-radius: 10px;
    background-color: #d53d3c;
    border: 1px solid #d53d3c;
    width: 100%;
    height: 45px;
    color: white;
    font-size: 18px;
`

export interface params{
    label: string;
    className?: string;
    style?: CSSProperties;
    onClick?: () => void
}

const App = (params:params):JSX.Element => {
    return(
        <Container className={ params.className } onClick={params.onClick}  >
            { params.label }
        </Container>
    )
}

App.defaultProps = {
    className: "button"
}

export default App;