import { styled } from "goober";
import Button from "./button"
import MiniCard from "./miniCard"

const Container = styled("div")<{image: string}>`

    position: relative;
    

    height: 100vh;
    width: 100vw;
    
    .mask{
        position: absolute;
        inset: 0;
        background-color: #0A0A0A24;
    }

    .body{
        height: 60vh;    

        background-color: #111114;
        padding: 15px;

        display: flex;
        flex-direction: column;
        gap: 20px;

        box-shadow: 0px -6px 6px #141414;        
    }

    .title{
        color: white;
        margin: 0px;
        font-size: 38px;
    }
    .description{
        color: white;
        margin: 0px;
        color: #CFCED1;
    }

    .swipe{
        display: flex;
        flex-direction: row;
        gap: 15px;
        overflow-x: auto;
    }

    .footer{
        margin-top: 10px;
        margin-bottom: 10px;
        display: flex;
        flex-direction: row;
        align-items: center;
        gap: 15px;
    }

    .header{
        position: relative;
        width: 100%;
        height: 40vh;

        background-image: url(${props => props.image});
        background-repeat: no-repeat;
        background-position: center;
        background-size: cover;
        background-color: #111114;
        

        .cover{
            position: absolute;
            bottom: 0px;
            width: 100%;
            display: flex;
            flex-direction: row;
            justify-content: space-around;
            background: linear-gradient(180deg, rgba(17,17,20,0) 0%, rgba(17,17,20,1) 100%);


            .stat{
                color: white;
                background-color: red;
            }
        }
    }

`

export interface params{
    image: string;
    onClick?: () => void
}

const App = (params:params):JSX.Element => {
    return (
      <Container image={params.image}>
        <div className="header">
            <div className="mask"></div>
            <div className="cover">
                <h2 className="title">Costa Coffee</h2>
                <div className="stat">4.5</div>
            </div>
        </div>

        <div className="body">
          <p className="description">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Eligendi rerum optio aut alias debitis laboriosam id est doloremque explicabo autem ratione nesciunt quo, nihil, tenetur obcaecati perferendis repudiandae. Enim, architecto!</p>

          <div className="footer">
            <p className="description">4 personas</p>
            <p className="description">Envío a domicilio</p>
          </div>

          <Button label={"Solicitar Producto"} />
          <div className="swipe">
            <MiniCard 
                image="/images/product.jpg" 
                title="Milanesas con pure"
                description="15% Off"
                onClick={ params.onClick }
            />
            <MiniCard
              image="/images/background2.jpg"
              title="Ensalada dietetica"
              description="15% Off"
              onClick={ params.onClick }
            />
            
          </div>
        </div>
      </Container>
    );
}

export default App;