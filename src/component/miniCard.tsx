import { styled } from "goober";

const Container = styled("div")<{ image: string }>`
    height: 200px;
    min-width: 200px;
    
    border-radius: 15px;

    position: relative;

    background-image: url(${props => props.image});
    background-repeat: no-repeat;

    background-position: center;
    background-size: cover;
    

    .mask{
        height: 45px;
        position: absolute;
        bottom: 0px;
        top: unset;
        background-color: #000000d1;
        padding: 10px;
        display: flex;
        flex-direction: column;
        gap: 5px;
    }

    .title{
        font-size: 14px;
        line-height: 15px;
        min-height: 25px;
    }

    .description{
        font-size: 12px;
    }
`

export interface params{
    image: string
    title: string
    description?: string
    onClick?: () => void;
}

const App = (params:params):JSX.Element => {
    return(
        <Container className="container" image={ params.image } onClick={ params.onClick } >
            <div className="mask">
                <h2 className="title">{ params.title }</h2>
                <p className="description">{ params.description }</p>
            </div>
        </Container>
    )
}

export default App;