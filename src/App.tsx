import { styled, } from 'goober';
import Card from "./component"
import Details from "./details"
import { useState } from 'react';



const Container = styled("div")`
  height: 100vh;

  --font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;
  --card-background-color:#2e2f30;
  --card-border: 1px solid #343536;
  --card-selected: #0098d924;

  *{
    font-family: var(--font-family);
    box-sizing: border-box;
  }

  .description{
    font-size: 12px;
  }

  .button{
    height: 45px;
    border-radius: 5px;
  }
`

const App = ():JSX.Element => {

  const [ visible, setVisible ] = useState<boolean>(false)
  console.log(visible)
  return (
    <Container>
      <Card image="/images/background2.jpg" onClick={() => setVisible(true)} />
      <Details
        price={450}
        visible={visible}
        image="/images/background2.jpg"
        title="Costa coffee"
        description="lorem ipsum dolor sit lure rerum, veritatis explicabo nemo facere providet quareat eligendi? Veritatis laborum ex sunt rerum omnis commodi."
        onClick={ () => setVisible(false) }
      />
    </Container>
  );
}

export default App